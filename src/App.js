/*import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
*/

import React, { Component } from 'react';
import Header from './components/Header';
import Form from './components/Form';
import Weather from './components/Weather';
import Places from './components/Places';
import * as googleMapsFetch from './api/googleMaps';

const API_KEY = '65d465b8c8f7cdc8a5a16dafab233620';

class App extends Component {
  
  state = {
    is_loadng: false,
    temperature: undefined,
    icon: undefined,
    city: undefined,
    country: undefined,
    humidity: undefined,
    description: undefined,
    places: undefined,
    error: undefined
  };
  
  //constructor() {
    //How to bind so you can use class methods. You need super().
    // super();
    // this.getWeather = this.getWeather.bind(this);
  //}
  
  //Can use arrow function or class method using constructor to bind
  getWeather = async (e) => {
    e.preventDefault();
    
    this.setState({
      is_loadng: true,
      temperature: undefined,
      icon: undefined,
      city: undefined,
      country: undefined,
      humidity: undefined,
      description: undefined,
      places: undefined,
      error: undefined
    });
    
    // console.log(e.target.elements.city.v);
    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    
    
    if(city && country){
      try {
        const api_call = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`);
        const data = await api_call.json();
        // console.log(data);
        const gm_data = await googleMapsFetch.default(data.name, data.coord.lat, data.coord.lon, data.weather[0].main);
        // console.log(gm_data);
        
        this.setState({
          isLoading: false,
          temperature: data.main.temp,
          icon: data.weather[0].icon,
          city: data.name,
          country: data.sys.country,
          humidity: data.main.humidity,
          description: data.weather[0].description,
          places: gm_data,
          error: data.message !== undefined ? data.message : ""
        });
      } catch (e) {
        this.setState({
          isLoading: false,
          temperature: undefined,
          icon: undefined,
          city: undefined,
          country: undefined,
          humidity: undefined,
          description: undefined,
          places: undefined,
          error: "Please provide a valid city and country."
        });
      }
    } else {
      this.setState({
        isLoading: false,
        temperature: undefined,
        icon: undefined,
        city: undefined,
        country: undefined,
        humidity: undefined,
        description: undefined,
        places: undefined,
        error: "Please provide a city and country."
      });
    }
  }
  
  render(){
    return (
      <div>
        <Header />
        <main role="main">
          <div className="container">
            <div className="row row-search">
              <div className="col-xs-12 text-center">
                <Form getWeather={this.getWeather} />
              </div>
            </div>
            <div className="row row-results">
              <div className="col-sm-4 col-weather-conditions">
              <Weather 
                is_loading={this.state.isLoading}
                temperature={this.state.temperature}
                icon={this.state.icon}
                city={this.state.city}
                country={this.state.country}
                humidity={this.state.humidity}
                description={this.state.description}
                error={this.state.error}
              />
              </div>
              <div className="col-sm-8 col-places">
                  { this.state.places && <Places is_loading={this.state.isLoading} place={this.state.places} />}
              </div>
            </div>
          </div>
        </main>
      </div>
    );
  }
}




export default App;