import React from 'react';

const Places = props => {
    // console.log(props.place.results[0]);
    
    
    return(
      
      <ul className="list-group">
      {
        props.isLoading && <i className="fa fa-circle-o-notch fa-spin"></i>
      }
        {props.place.results.map((item, i) => {
          return (<li className="list-group-item" key={i}>
            
              <h2>{item.name}</h2>
              {item.opening_hours && <p>{item.opening_hours.open_now && 'Open Now'}{!item.opening_hours.open_now && 'Closed'}</p>}
              {item.formatted_address && <p>Address: {item.formatted_address}</p>}
              {item.rating && <p>Rating: {item.rating}</p>}
            
          </li>)})}
      </ul>
    );
}

export default Places;