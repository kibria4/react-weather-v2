import React from 'react';

const Weather = props => (
  <div className="weather__info">
    {
      props.is_loading && <i className="fa fa-circle-o-notch fa-spin"></i>
    }
    {
      props.icon && <img src={`http://openweathermap.org/img/w/${props.icon}.png`} alt="" />
    }
    { 
      props.error && <p className="weather__key">{ props.error }</p> 
    }
    { 
      props.city && props.country && <p className="weather__key">Location: 
      <span className="weather__value">{props.city}, {props.country}</span>
      </p> 
    }  
    { 
      props.temperature && <p className="weather__key">Temperature: 
      <span className="weather__value">{props.temperature} degrees celcius</span>
      </p> 
    }
    { 
      props.description && <p className="weather__key"><span>{props.description}</span></p> 
    }
    { 
      props.humidity && <p className="weather__key">Humidity: 
        <span className="weather__value">{props.humidity}%</span>
      </p> 
    }
  </div>
);

export default Weather;