import React from 'react';

const Header = props => {
  return (
    <header>
      <div className="container">
        <div className="row">
          <div className="col-sm-6 col-logo">
            <h1>Instaweather</h1>
          </div>
          <div className="col-sm-6 col-icons">
          <ul className="list-inline pull-right text-right">
            <li>
              <a href="/">
              Home
              </a>
            </li>
            <li>
              <a href="/about">
              About
              </a>
            </li>
          </ul>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;