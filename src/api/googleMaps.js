import * as googleMaps from '@google/maps';

const GM_API_KEY = 'AIzaSyCe3YuvIUTb_xwA55naHU4WAgijoqD_j3U';

googleMaps.createClient({
  key: GM_API_KEY
});

const fetchPlaces = (location, lat, long, condition) => {
  var searchPlaces;
  // if not any of these rain, thunderstorm, snow, mist
  switch(condition){
    case 'Rain':
    case 'Thunderstorm':
    case 'Snow':
    case 'Mist':
      searchPlaces = indoorActivitiesSearch(location, lat, long);
      return searchPlaces;
    default:
      searchPlaces = outdoorActivitiesSearch(location, lat, long);
      return searchPlaces;
  }
}

const indoorActivitiesSearch = async (location, lat, long) => {
  const api_call = await fetch(`https://maps.googleapis.com/maps/api/place/textsearch/json?query=indoor+activities+in+${location}&location=${lat},${long}&radius=12000&key=${GM_API_KEY}`);
  const data = await api_call.json();
  // console.log(data);
  return data;
}

const outdoorActivitiesSearch = async (location, lat, long) => {
  const api_call = await fetch(`https://maps.googleapis.com/maps/api/place/textsearch/json?query=outdoor+activities+in+${location}&location=${lat},${long}&radius=12000&key=${GM_API_KEY}`);
  const data = await api_call.json();
  // console.log(data);
  return data;
}

export default fetchPlaces;
